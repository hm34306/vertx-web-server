package playground.vertx_webserver;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.file.FileSystem;
import io.vertx.core.http.HttpServer;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.CorsHandler;
import io.vertx.ext.web.handler.StaticHandler;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Hello world!
 *
 */
public class VertxWebServer extends AbstractVerticle
{
	
    @Override
    public void start() throws IOException{
    	
    	String content = new String(Files.readAllBytes(Paths.get("config/web.config")));
    	JsonObject config = new JsonObject(content);
    	
    	HttpServer server = vertx.createHttpServer();

    	Router router = Router.router(vertx);

    	if(config.getBoolean("handleCors")){
	    	router.route().handler(CorsHandler.create("*")
		    	.allowedMethod(io.vertx.core.http.HttpMethod.GET)
		    	.allowedMethod(io.vertx.core.http.HttpMethod.POST)
		    	.allowedMethod(io.vertx.core.http.HttpMethod.OPTIONS)
		    	.allowedHeader("Access-Control-Request-Method")
		    	.allowedHeader("Access-Control-Allow-Credentials")
		    	.allowedHeader("Access-Control-Allow-Origin")
		    	.allowedHeader("Access-Control-Allow-Headers")
		    	.allowedHeader("Content-Type"));
    	}
    	

    	router.route().handler(StaticHandler.create(content)
    			.setWebRoot(config.getString("webroot"))
    			.setCachingEnabled(false));

    	server.requestHandler(router::accept).listen( config.getInteger( "port" ));
    	
    	System.out.println("Server started");
    }
}
