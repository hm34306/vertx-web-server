package playground.vertx_webserver;

import io.vertx.core.DeploymentOptions;
import io.vertx.core.Launcher;
import io.vertx.core.Vertx;

public class WebServerLauncher extends Launcher {

	public static void main(String[] args){
		Vertx vertx = Vertx.vertx();
	    vertx.deployVerticle( new VertxWebServer() , new DeploymentOptions());
	}
}
