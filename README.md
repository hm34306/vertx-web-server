# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* A quick Development web serve for serving static html and handling cors
* 1.0.0
* [vert.x](http://vertx.io)
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)


### How do I get set up? ###

* Install
* mvn install produces a fat jar
* Configuration - config/web.config is small json file allowing you to set the port and enable cors management NOTE: webroot can only be relative
* Run - java -jar vertx-webserver-1.0.0-jar-with-dependencies.jar
* Dependencies - jdk 1.8
* How to run tests - no tests at this time
* Deployment instructions - can deploy anywhere, as long as you have java 1.8. This is not a full fledged server, but one to quickly stand up and develop with.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact